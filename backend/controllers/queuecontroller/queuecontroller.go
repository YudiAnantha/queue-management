package queuecontroller

import (
	"net/http"
	"strconv"

	"example/backend/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Index(c *gin.Context) {
	var queues []models.Queue

	models.DB.Find(&queues)
	c.JSON(http.StatusOK, gin.H{"queues": queues})
}

func Show(c *gin.Context) {
	var queue models.Queue
	id := c.Param("id")

	if err := models.DB.First(&queue, id).Error; err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Data tidak ditemukan"})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{"queue": queue})
}

func Create(c *gin.Context) {
	var queue models.Queue

	if err := c.ShouldBindJSON(&queue); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	models.DB.Create(&queue)

	c.JSON(http.StatusOK, gin.H{"queue": queue})
}

func Update(c *gin.Context) {
	var queue models.Queue
	id := c.Param("id")

	if err := c.ShouldBindJSON(&queue); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	if models.DB.Model(&queue).Where("id = ?", id).Updates(&queue).RowsAffected == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "tidak dapat mengupdate queue"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Data berhasil diperbarui"})
}

func Delete(c *gin.Context) {
	var queue models.Queue

	input := map[string]string{"id": "0"}
	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	id, _ := strconv.ParseInt(input["id"], 10, 64)
	if models.DB.Delete(&queue, id).RowsAffected == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Tidak dapat menghapus queue"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Data berhasil dihapus"})
}
