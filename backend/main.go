package main

import (
	"example/backend/controllers/queuecontroller"
	"example/backend/models"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	models.ConnectDatabase()

	r.GET("/queues", queuecontroller.Index)
	r.GET("/queue/:id", queuecontroller.Show)
	r.POST("/queue", queuecontroller.Create)
	r.PUT("/queue", queuecontroller.Update)
	r.DELETE("/queue", queuecontroller.Delete)

	r.Run()
}
