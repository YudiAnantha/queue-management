package models

import "time"

type Queue struct {
	ID uint `gorm:"primaryKey" json:"-"`
	// Type      Type      `gorm:"embedded"`
	Medical_type int       `gorm:"type:int" json:"medical_type"`
	Counter_1    int       `gorm:"type:int" json:"counter_1"`
	Counter_2    int       `gorm:"type:int" json:"counter_2"`
	Counter_3    int       `gorm:"type:int" json:"counter_3"`
	CreatedAt    time.Time `gorm:"autoCreateTime" json:"-"`
}
