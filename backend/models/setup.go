package models

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDatabase() {
	database, err := gorm.Open(mysql.Open("root:root@tcp(localhost:3306)/queue_db1"))
	if err != nil {
		panic(err)
	}
	database.AutoMigrate(&Queue{})

	DB = database
}
