package models

type Type struct {
	ID   uint   `gorm:"primaryKey" json:"-"`
	Name string `json:"-"`
}
