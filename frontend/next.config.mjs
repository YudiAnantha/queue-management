/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        'MYSQL_HOST':'localhost',
        'MYSQL_PORT':'3306',
        'MYSQL_DATABASE':'queue_db1',
        'MYSQL_USER':'root',
        'MYSQL_PASSWORD':'root',
    }
};

export default nextConfig;
