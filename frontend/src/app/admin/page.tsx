"use client"
import React from 'react'
import { useEffect, useState } from "react"

const page = () => {

  const [data, setData] = useState([])
  const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    fetch('http://localhost:8080/queues', {mode: 'no-cors'})
      .then((res) => res.json())
      .then((data) => {
        setData(data)
        setLoading(false)
        console.log('111')
      })
  }, [])

  return (
    <div>
      {data.map((queue:any) => (<p>{queue.medical_type}</p>) )}
    </div>
  )
}

export default page