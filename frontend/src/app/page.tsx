export default function Home() {
  return (
    <main>
      {/* background image */}
      <div className="relative h-screen w-screen page-background"></div>
      {/* content */}
      <div className="absolute top-0 left-0 h-screen w-screen">
        {/* Current Queue */}
        <div className="h-1/6 w-full grid grid-cols-3 gap-5 p-5">
          <div className="flex flex-col justify-center items-center text-white bg-cyan-500 p-3 border border-gray-200 shadow-md rounded-lg">
            <h1>Medical Checkup (A)</h1>
            <p className="text-xl font-bold">10</p>
          </div>
          <div className="flex flex-col justify-center items-center text-white bg-red-400 p-3 border border-gray-200 shadow-md rounded-lg">
              <h1>Specialist (B)</h1>
              <p className="text-xl font-bold">15</p>
          </div>
          <div className="flex flex-col justify-center items-center text-white bg-green-400 p-3 border border-gray-200 shadow-md rounded-lg">
              <h1>Assurance (C)</h1>
              <p className="text-xl font-bold">5</p>
            </div>
        </div>
        {/* Queue Form */}
        <div className="h-3/6 w-full flex flex-row justify-center items-center">
          <div className="h-5/6 w-1/3 border border-gray-200 shadow-xl rounded-lg cursor-pointer transition ease-in-out hover:-translate-y-1 hover:-translate-x-1 hover:scale-110 duration-300">
            <img src="https://images.unsplash.com/photo-1586773860418-d37222d8fce3" alt="" className="w-full h-1/2 object-cover rounded-t-lg" />
            <div className="h-1/2 flex flex-col justify-between p-5 bg-white rounded-b-lg">
              <h1 className="font-bold">Queue Ticket</h1>
              <form action="" className="h-3/4 flex flex-col justify-between rounded-b-lg">
                <p>Please select your service and click "submit" button</p>
                <div className="flex justify-between rounded-lg">
                  <select name="" id="" className="bg-gray-300 px-3 py-2 rounded-lg shadow-lg">
                    <option value="">Select service</option>
                  </select>
                  <button className="bg-blue-300 px-3 py-2 rounded-lg shadow-lg">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Queue List */}
        <div className="h-2/6 w-full p-5 overflow-y-auto">
          {/* Table */}
          <table className="table-auto w-full bg-white border-collapse border border-slate-300 rounded-lg overflow-hidden">
            <thead>
              <tr>
                <th className="border border-slate-300">No</th>
                <th className="border border-slate-300">Queue</th>
                <th className="border border-slate-300">Type</th>
                <th className="border border-slate-300">Status</th>
                <th className="border border-slate-300">Time</th>
              </tr>
            </thead>
            <tbody>
              <tr className="bg-cyan-100">
                <td className="border border-slate-300">1</td>
                <td className="border border-slate-300">1A</td>
                <td className="border border-slate-300">
                  <div className="font-bold">
                    Medical Checkup
                  </div>
                </td>
                <td className="border border-slate-300">
                  <div className="flex items-center">
                    <div className="bg-yellow-400 status-circle rounded-full mr-2"></div>
                    <p>waiting...</p>
                  </div>
                </td>
                <td className="border border-slate-300">10:00</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </main>
  )
}
